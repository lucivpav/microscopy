# Microscopy source repository
This repository contains several methods for processing Whole Slide Images (WSI) - microscopy images of stained and colored tissue samples for histopathological analysis.
The repository provides functionality in
  1. I/O Data handling `microscopyio.slide_image` file, `SlideImage` class and its derivatives
  2. Scripts from


## Components

### Data handling
Currently (multi-level) .tif and the .ndpi format are supported as image inputs. Main purpose of the SlideImage class is to provide interface for
reading image patches from WSIs, with annotations if provided. These files contain the image data at different zooming levels (resolutions), 0th level
is the highest resolution. Patches are loaded by specifying the coordinates of the top-left corner, the size and the zooming level of the patch.
The coordinates are bound to level 0.

### Feature extraction
Two main files - `structural.py` and `textural.py` for extracting feature descriptors.

### Classification
Supervised, patch-based classification for PETACC and CAMELYON.

### Deep learning
First attempts to deep-learning, with `tflearn` and `tensorflow`. Contains a data-loader with the tflearn.Preloader API interface, that
can be plugged into tflearn for on-demand data loading.

### Scripts
Example scripts for extracting patches for a whole study and for feature extraction over these patches on the SGE grid.


## How to use

### Requirements:

Configure a (virtual) python environment with the `requirements.txt` script provided in the top directory of this repository

### Avaialable data
WSI collections are currently available from two sources, the common root for all microscopic data is `/datagrid/Medical/microscopy`

 * **petacc3**: four batches of data in NDPI format, respective annotations are stored in <FILENAME>.NDPI.NDPA, annotations mark the tumor area
 * **CAMELYON16**: data from the CAMELYON challenge, year 2016, fully annotated tumor areas in the tumor training data (./training/tumor), annotation data are available in the `Train-Ground_Truth` folder, all data have XML, some of them also Masks. In the Readme.md file, there is a list of datasets that are not fully annotated (i.e. not all tumor areas are marked)
 * **CAMELYEON17**: 2017 edition of the challenge, data only sparsely annotated (only some data have small annotated areas in XML format in `training/annotations`), there are slice-level annotations in `training/stage_labels.csv`,
